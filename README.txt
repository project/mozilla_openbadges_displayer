Overview
-----------------------------------------------------------------
This module creates a block 'Mozilla openbadges displayer block' 
for display mozilla backpack. This block display all 
groups and its badges of logged in user which are public at 
mozilla backpack.

And user can also see the other user public badges on their profile.

Mozilla user badges updates every after 20 mins due to cache the user 
badges on email basis.

Limitations
------------

User account has been registered on Mozilla Openbadges Site with same email.

Installation
-------------

* Copy the whole "mozilla_openbadges_displayer" directory to 
your modules directory.

* Enable the module.

* Set your desired block configuration at "admin/structure/block".
