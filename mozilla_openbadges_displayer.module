<?php

/**
 * @file
 * @brief Mozilla Openbadges Display module file
 * Display user groups created in mozilla openbadges along with badges
 */

/**
 * Implements hook_block_info().
 */
function mozilla_openbadges_displayer_block_info() {
  global $base_url;
  $blocks['user_mozilla_badges'] = array('info' => t('User Mozilla Badges'), 'cache' => DRUPAL_NO_CACHE);
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function mozilla_openbadges_displayer_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'user_mozilla_badges':
      $block = mozilla_openbadges_displayer_user_block();
      break;
  }
  return $block;
}

/**
 * Logged in user mozilla public badges.
 */
function mozilla_openbadges_displayer_user_block() {
  global $user;
  $block = array();
  if ($user->uid > 0) {
    $mozilla_badges = mozilla_openbadges_displayer_group_badges($user);
    if (!empty($mozilla_badges)) {
      $block['subject'] = t("!user Badges", array('!user' => $user->name));
      $block['content'] = theme('user_mozilla_badges', array('mozilla_badges' => $mozilla_badges));
    }
  }
  return $block;
}

/**
 * Calls diffrent mozilla backpack api.
 */
function mozilla_openbadges_displayer_group_badges($account = NULL) {
  global $user;
  $backpackdata = array();
  if (is_object($account)) {
    $object = $account;
  } else {
    $object = $user;
  }
  if ($object->uid > 0) {
    $email = $object->mail;
    $user_cache = cache_get('user-mozilla-badges:' . md5($email), 'cache');
    if (is_object($user_cache)) {
      return $user_cache->data;
    }
    $data = 'email=' . $email;
    $options = array(
      'method' => 'POST',
      'data' => $data,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );
    $result = drupal_http_request('http://beta.openbadges.org/displayer/convert/email', $options);
    $body = json_decode($result->data);
    if ($body->status !== 'missing') {
      $groupsurl = "http://beta.openbadges.org/displayer/" . $body->userId . "/groups.json";
      $groupsjson = file_get_contents($groupsurl, 0, NULL, NULL);
      $groupsdata = json_decode($groupsjson);

      $backpackdata = array();
      foreach ($groupsdata->groups as $group) {
        $badgesurl = "http://beta.openbadges.org/displayer/" . $body->userId . "/group/" . $group->groupId . ".json";
        $badgesjson = file_get_contents($badgesurl, 0, NULL, NULL);
        $badgesdata = json_decode($badgesjson);

        $badgesingroup = array();
        if (is_array($badgesdata->badges)) {
          foreach ($badgesdata->badges as $badge) {
            $badgedata = array(
              'title' => $badge->assertion->badge->name,
              'image' => $badge->imageUrl,
              'criteriaurl' => $badge->assertion->badge->criteria,
              'issuername' => $badge->assertion->badge->issuer->name,
              'issuerurl' => $badge->assertion->badge->issuer->origin,
            );
            array_push($badgesingroup, $badgedata);
          }
        }
        $groupdata = array(
          'groupname' => $group->name,
          'groupID' => $group->groupId,
          'numberofbadges' => $group->badges,
          'badges' => $badgesingroup,
        );
        array_push($backpackdata, $groupdata);
      }
    }
    cache_set('user-mozilla-badges:' . md5($email), $backpackdata, 'cache', time() + 1800);
  }
  return $backpackdata;
}

/**
 * Implements hook_entity_view().
 */
function mozilla_openbadges_displayer_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'user' && $view_mode == 'full') {
    $mozilla_badges = mozilla_openbadges_displayer_group_badges($entity);
    if (!empty($mozilla_badges)) {
      $entity->content['mozilla_badges'] = array(
        '#title' => t('Badges'),
        '#markup' => theme('user_mozilla_badges', array('mozilla_badges' => $mozilla_badges)),
        '#weight' => 10,
      );
    }
  }
}

/**
 * Implements hook_theme().
 */
function mozilla_openbadges_displayer_theme() {
  return array(
    'user_mozilla_badges' => array(
      'variables' => array(
        'mozilla_badges' => NULL,
      ),
      'template' => 'user-mozilla-badges',
    ),
  );
}

/**
 * Implements hook_init().
 */
function mozilla_openbadges_displayer_init() {
  drupal_add_css(drupal_get_path('module', 'mozilla_openbadges_displayer') . '/css/mozilla_openbadges_displayer.css');
}
